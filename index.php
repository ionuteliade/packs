<?php

$packs = [5000, 2000, 1000, 500, 250];

function order($a, $b)
{
    $new_order = 0;
    $no_of_packs = 0;
    foreach($b as $pack):
        $no_of_packs = (end($b) == $pack) ? ceil($a/$pack) : floor($a/$pack);
        $a = $a - ($no_of_packs * $pack);
        $new_order = $new_order + ($no_of_packs * $pack);
    endforeach;
    echo "Send ".$new_order ." widgets<br>";

    foreach($b as $pack):
        $no_of_packs = (end($b) == $pack) ? ceil($new_order/$pack) : floor($new_order/$pack);
        $pack_s = ($no_of_packs == 1) ? "pack" : "packs";
        $new_order = $new_order - ($no_of_packs * $pack);
        if($no_of_packs > 0) { echo $no_of_packs." ".$pack_s." x ".$pack." widgets<br>";}
    endforeach;
}

if(isset($_POST['order'])) 
{
    if((int)($_POST['order']) > 0)
    {
        echo "Order: ".$_POST['order']." widgets<br>";
        order($_POST['order'], $packs);
    }
    else
    {
        echo "Insert must be a number > 0";
    }
}

?>

<hr>Insert number of widgets<br>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
    <input type="text" name="order" required>
    <input type="submit" value="Calculate">
</form>
